package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;
import ru.tsc.kirillov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT,
            ArgumentConst.ABOUT,
            "Отображение информации о разработчике."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION,
            ArgumentConst.VERSION,
            "Отображение версии программы."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP,
            ArgumentConst.HELP,
            "Отображение доступных команд."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT,
            null,
            "Закрытие приложения."
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO,
            ArgumentConst.INFO,
            "Отображение информации о системе."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS,
            ArgumentConst.COMMANDS,
            "Отображение списка команд."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS,
            ArgumentConst.ARGUMENTS,
            "Отображение списка аргументов."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE,
            null,
            "Создание новой задачи."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST,
            null,
            "Отображение списка задач."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR,
            null,
            "Удалить все задачи."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID,
            null,
            "Отобразить задачу по её ID."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX,
            null,
            "Отобразить задачу по её индексу."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID,
            null,
            "Обновить задачу по её ID."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX,
            null,
            "Обновить задачу по её индексу."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID,
            null,
            "Удалить задачу по её ID."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX,
            null,
            "Удалить задачу по её индексу."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE,
            null,
            "Создание нового проекта."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST,
            null,
            "Отображение списка проектов."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR,
            null,
            "Удалить все проекты."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID,
            null,
            "Отобразить проект по его ID."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX,
            null,
            "Отобразить проект по его индексу."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID,
            null,
            "Обновить проект по его ID."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX,
            null,
            "Обновить проект по его индексу."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID,
            null,
            "Удалить проект по его ID."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX,
            null,
            "Удалить проект по его индексу."
    );

    private final static Command[] terminalCommands = new Command[] {
            ABOUT, VERSION, HELP, INFO,
            COMMANDS, ARGUMENTS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            EXIT
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }

}
