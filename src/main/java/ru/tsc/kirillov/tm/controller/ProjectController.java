package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.IProjectController;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjectList() {
        System.out.println("[Список проектов]");
        final List<Project> projects = projectService.findAll();
        for(final Project project: projects) {
            if (project == null)
                continue;
            System.out.println(project);
        }
        System.out.println("[Конец списка]");
    }

    @Override
    public void clearProject() {
        System.out.println("[Очистка списка проектов]");
        projectService.clear();
        System.out.println("[Список успешно очищен]");
    }

    @Override
    public void createProject() {
        System.out.println("[Создание проекта]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null)
            System.out.println("[Ошибка при создании проекта]");
        else
            System.out.println("[Проект успешно создан]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[Удаление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.removeByIndex(fixIndex(index));
        if (project == null)
            System.out.printf("[Проект по индексу `%d` не найден]\n", index + 1);
        else
            System.out.println("[Проект успешно удален]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[Удаление проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null)
            System.out.printf("[Проект по ID = `%s` не найден]\n", id);
        else
            System.out.println("[Проект успешно удален]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[Отображение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(fixIndex(index));
        if (project == null) {
            System.out.printf("[Проект по индексу `%d` не найден]\n", index);
            return;
        }
        showProject(project);
        System.out.println("[Успешно отображено]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[Отображение проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.printf("[Проект по ID = `%s` не найден]\n", id);
            return;
        }
        showProject(project);
        System.out.println("[Успешно отображено]");
    }

    private void showProject(Project project) {
        if (project == null)
            return;
        System.out.println("ID: " + project.getId());
        System.out.println("Имя: " + project.getName());
        System.out.println("Описание: " + project.getDescription());
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[Обновление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();

        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateByIndex(fixIndex(index), name, description);
        if (project == null)
            System.out.println("[Ошибка при обновлении проекта]");
        else
            System.out.println("[Проект успешно обновлена]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[Обновление проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateById(id, name, description);
        if (project == null)
            System.out.println("[Ошибка при обновлении проекта]");
        else
            System.out.println("[Проект успешно обновлен]");
    }

    private Integer fixIndex(Integer index) {
        return --index;
    }

}
