package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.ITaskController;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[Список задач]");
        final List<Task> tasks = taskService.findAll();
        int idx = 0;
        for(final Task task: tasks) {
            if (task == null)
                continue;
            System.out.println(++idx + "." + task);
        }
        System.out.println("[Конец списка]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[Очистка списка задач]");
        taskService.clear();
        System.out.println("[Список успешно очищен]");
    }

    @Override
    public void createTask() {
        System.out.println("[Создание задачи]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.create(name, description);
        if (task == null)
            System.out.println("[Ошибка при создании задачи]");
        else
            System.out.println("[Задача успешно создана]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[Удаление задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.removeByIndex(fixIndex(index));
        if (task == null)
            System.out.printf("[Задача по индексу `%d` не найдена]\n", index);
        else
            System.out.println("[Задача успешно удалена]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[Удаление задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null)
            System.out.printf("[Задача по ID = `%s` не найдена]\n", id);
        else
            System.out.println("[Задача успешно удалена]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[Отображение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(fixIndex(index));
        if (task == null) {
            System.out.printf("[Задача по индексу `%d` не найдена]\n", index);
            return;
        }
        showTask(task);
        System.out.println("[Успешно отображено]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[Отображение задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.printf("[Задача по ID = `%s` не найдена]\n", id);
            return;
        }
        showTask(task);
        System.out.println("[Успешно отображено]");
    }

    private void showTask(Task task) {
        if (task == null)
            return;
        System.out.println("ID: " + task.getId());
        System.out.println("Имя: " + task.getName());
        System.out.println("Описание: " + task.getDescription());
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[Обновление задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();

        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.updateByIndex(fixIndex(index), name, description);
        if (task == null)
            System.out.println("[Ошибка при обновлении задачи]");
        else
            System.out.println("[Задача успешно обновлена]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[Обновление задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();

        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.updateById(id, name, description);
        if (task == null)
            System.out.println("[Ошибка при обновлении задачи]");
        else
            System.out.println("[Задача успешно обновлена]");
    }

    private Integer fixIndex(Integer index) {
        return --index;
    }

}
