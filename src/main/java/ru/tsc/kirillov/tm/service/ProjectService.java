package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null)
            return null;
        return projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty())
            return null;
        if (description == null || description.isEmpty())
            return null;
        return projectRepository.create(name, description);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project remove(final Project project) {
        projectRepository.remove(project);
        return project;
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0 || index >= projectRepository.count())
            return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty())
            return null;
        if (name == null || name.isEmpty())
            return null;

        final Project project = findOneById(id);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0 || index >= projectRepository.count())
            return null;
        if (name == null || name.isEmpty())
            return null;

        final Project project = findOneByIndex(index);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);

        return project;
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty())
            return null;
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0 || index >= projectRepository.count())
            return null;
        return projectRepository.removeByIndex(index);
    }

}
