package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

    Integer count();

}
