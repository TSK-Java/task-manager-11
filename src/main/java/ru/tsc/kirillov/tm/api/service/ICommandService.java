package ru.tsc.kirillov.tm.api.service;

import ru.tsc.kirillov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
