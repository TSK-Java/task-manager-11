package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
