package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task create(String name, String description);

    Task add(Task task);

    void clear();

    Integer count();

}
